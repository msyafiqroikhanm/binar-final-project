const router = require("express").Router();
const SellerController = require("../controllers/seller.controller");
const UserMiddleware = require("../middlewares/user.middleware");
const AuthMiddleware = require("../middlewares/auth.middlewares");
const { check } = require("express-validator");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const UserController = require("../controllers/user.controller");
const multer = require("multer");
const storage = require("../services/multerstorage");
const upload = multer({
  storage,
  fileFilter: (req, file, cb) => {
    if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
      cb(null, true);
    } else {
      cb(
        {
          status: 400,
          message: "File type does not match",
        },
        false
      );
    }
  },
});

router.get(
  "/profile",
  AuthMiddleware.authentication,
  UserController.getDetailUser
);

router.put(
  "/update",
  AuthMiddleware.authentication,
  upload.single("profile_picture"),
  UserController.updateUser
);

router.get(
  "/products",
  AuthMiddleware.authentication,
  UserMiddleware.checkCompleteUserInfo,
  SellerController.getProducts
);

router.post(
  "/products",
  AuthMiddleware.authentication,
  UserMiddleware.checkCompleteUserInfo,
  upload.array("image", 4),
  ValidationMiddleware.multipleFile,
  [
    check("name", "Nama tidak boleh kosong").notEmpty(),
    check("price", "Kota tidak boleh kosong").notEmpty(),
    check("description", "Alamat tidak boleh kosong").notEmpty(),
  ],
  ValidationMiddleware.result,
  SellerController.createProduct
);

router.put(
  "/products/:id",
  AuthMiddleware.authentication,
  UserMiddleware.checkCompleteUserInfo,
  SellerController.updatePublish
);

router.put(
  "/products/:id/update",
  AuthMiddleware.authentication,
  UserMiddleware.checkCompleteUserInfo,
  upload.array("image", 4),
  SellerController.updateProduct
);

router.get(
  "/products/:id",
  AuthMiddleware.authentication,
  UserMiddleware.checkCompleteUserInfo,
  SellerController.getDetailProduct
);

router.get(
  "/products/interests/:id",
  AuthMiddleware.authentication,
  UserMiddleware.checkCompleteUserInfo,
  SellerController.getDetailInterestedProduct
);

router.put(
  "/products/interests/:id",
  AuthMiddleware.authentication,
  UserMiddleware.checkCompleteUserInfo,
  [check("agreement", "Atribut agreement harus berupa boolean").isBoolean()],
  ValidationMiddleware.result,
  SellerController.transactionInterestedProduct
);

router.put(
  "/products/interests/:id/status",
  AuthMiddleware.authentication,
  UserMiddleware.checkCompleteUserInfo,
  [check("status", "Atribut status harus berupa boolean").isBoolean()],
  ValidationMiddleware.result,
  SellerController.updateStatusProduct
);

router.delete(
  "/products/delete/:id",
  AuthMiddleware.authentication,
  SellerController.deleteProduct
)

module.exports = router;
