const WishlistController = require("../controllers/wishlist.controller");
const AuthMiddleware = require("../middlewares/auth.middlewares");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const { check } = require("express-validator");
const router = require("express").Router();

router.post(
  "/",
  AuthMiddleware.authentication,
  [
    check(
      "productId",
      "Atribut productId tidak boleh kosong dan harus berupa integer"
    ).isNumeric(),
  ],
  ValidationMiddleware.result,
  WishlistController.createWishlist
);

router.get("/", AuthMiddleware.authentication, WishlistController.listWishlist);

router.get(
  "/:id",
  AuthMiddleware.authentication,
  WishlistController.detailWishlist
);

router.delete(
  "/:id",
  AuthMiddleware.authentication,
  WishlistController.deleteWishlist
);

router.delete(
  "/products/:id",
  AuthMiddleware.authentication,
  WishlistController.deleteWishlistByProduct
);

module.exports = router;
