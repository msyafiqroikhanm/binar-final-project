const router = require("express").Router();
const authRoutes = require("./auth.route");
const sellerRoutes = require("./seller.route");
const buyerRoutes = require("./buyer.route");
const notificationRoutes = require("./notifications.route");

router.get("/", (req, res, next) => res.send("Halo dunia"));
router.use("/auth", authRoutes);
router.use("/sellers", sellerRoutes);
router.use("/buyers", buyerRoutes);
router.use("/notifications", notificationRoutes);

module.exports = router;
