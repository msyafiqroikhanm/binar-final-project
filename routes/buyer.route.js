const router = require("express").Router();
const BuyerController = require("../controllers/buyer.controller");
const UserMiddleware = require("../middlewares/user.middleware");
const AuthMiddleware = require("../middlewares/auth.middlewares");
const ValidationMiddleware = require("../middlewares/validation.middleware");
const { check } = require("express-validator");
const wishlistRoutes = require("./wishlist.route");

router.get(
  "/products/:id",
  AuthMiddleware.authentication,
  BuyerController.getProduct
);

router.post(
  "/products/:id/offer",
  AuthMiddleware.authentication,
  UserMiddleware.checkCompleteUserInfo,
  [check("price", "Atribut price harus berupa numeric").isNumeric()],
  ValidationMiddleware.result,
  BuyerController.offerPrice
);

router.get(
  "/products",
  (req, res, next) => {
    if (req.headers.authorization) {
      AuthMiddleware.authentication(req, res, next);
    } else {
      next();
    }
  },
  BuyerController.getAllProducts
);

router.use("/wishlists", wishlistRoutes);

module.exports = router;
