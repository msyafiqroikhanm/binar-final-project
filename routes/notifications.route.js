const NotifactionController = require("../controllers/notification.controller");
const SellerController = require("../controllers/seller.controller");
const AuthMiddleware = require("../middlewares/auth.middlewares");

const router = require("express").Router();

router.get(
  "/",
  AuthMiddleware.authentication,
  NotifactionController.getNotifactions
);

router.put(
  "/:id/read-notif",
  AuthMiddleware.authentication,
  NotifactionController.ReadNotification
);

router.put(
  "/read-notif-all",
  AuthMiddleware.authentication,
  NotifactionController.ReadAllNotification
);

module.exports = router;
