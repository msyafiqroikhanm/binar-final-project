const AuthController = require("../controllers/auth.controller");
const router = require("express").Router();
const { check } = require("express-validator");
const ValidationMiddleware = require("../middlewares/validation.middleware");

router.post(
  "/register",
  [
    check("name", "Nama tidak boleh kosong").notEmpty(),
    check(
      "email",
      "Email tidak boleh kosong dan harus sesuai format"
    ).isEmail(),
    check(
      "password",
      "Password minimal 8 karakter dan terdapat huruf besar dan kecil"
    ).isStrongPassword({
      minLength: 8,
      minLowercase: 1,
      minSymbols: 0,
    }),
  ],
  ValidationMiddleware.result,
  AuthController.register
);

router.post(
  "/login",
  [
    check("email", "Email tidak boleh kosong").notEmpty(),
    check("password", "Password tidak boleh kosong").notEmpty(),
  ],
  ValidationMiddleware.result,
  AuthController.login
);

router.get("/login/google", ValidationMiddleware.result, AuthController.oauth);

router.post(
  "/login/googlev2/",
  [check("id_token", "Atribut id_token tidak boleh kosong").notEmpty()],
  ValidationMiddleware.result,
  AuthController.googleOauth
);

module.exports = router;
