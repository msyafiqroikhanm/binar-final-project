'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Notifactions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Notifactions.belongsTo(models.Product,{ 
        foreignKey: "productId",
        as: "NotifactionsProduct" 
      })

      Notifactions.belongsTo(models.InterestedProduct,{ 
        foreignKey: "InterestedProductId", 
        as: "NotifactionsInterestedProduct"
       })

      Notifactions.belongsTo(models.User,{ 
        foreignKey: "userIdBuyer", 
        as: "NotifactionsBuyer" 
      })

      Notifactions.belongsTo(models.User,{ 
        foreignKey: "userIdSeller", 
        as: "NotifactionsSeller" 
      })
    }
  }
  Notifactions.init({

    // userId: DataTypes.INTEGER,
    userIdBuyer: DataTypes.INTEGER,
    userIdSeller: DataTypes.INTEGER,
    productId: DataTypes.INTEGER,
    InterestedProductId: DataTypes.INTEGER,
    Redirect: DataTypes.STRING,
    price: DataTypes.BIGINT,
    status: DataTypes.STRING,
    WatchedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Notifactions',
  });
  return Notifactions;
};