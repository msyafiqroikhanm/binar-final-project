"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Product, { foreignKey: "userId", as: "Product" });

      User.hasMany(models.Notifactions,{ 
        foreignKey: "userIdBuyer", 
        as: "BuyerNotifactions" 
      });

      User.hasMany(models.Notifactions,{ 
        foreignKey: "userIdSeller", 
        as: "SellerNotifactions" 
      });

      User.hasMany(models.InterestedProduct, {
        foreignKey: "buyerId",
        as: "InterestedProduct",
      });

      User.hasMany(models.Wishlist, { foreignKey: "userId", as: "Wishlist" });
    }
  }
  User.init(
    {
      nama: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      kota: DataTypes.STRING,
      alamat: DataTypes.TEXT,
      no_hp: DataTypes.STRING,
      profile_picture: DataTypes.STRING,
      completed: DataTypes.BOOLEAN,
      login_type: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
