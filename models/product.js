"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Product.hasMany(models.ProductCategory, {
        foreignKey: "productId",
        as: "ProductCategory",
      });

      Product.hasMany(models.ProductImage, {
        foreignKey: "productId",
        as: "ProductImage",
      });

      Product.hasMany(models.InterestedProduct, {
        foreignKey: "productId",
        as: "Interested",
      });
      
      Product.hasMany(models.Notifactions, {
        foreignKey: "productId",
        as: "ProductNotifactions",
      })

      Product.belongsTo(models.User, { foreignKey: "userId", as: "User" });

      Product.hasMany(models.Wishlist, {
        foreignKey: "productId",
        as: "Wishlist",
      });
    }
  }
  Product.init(
    {
      userId: DataTypes.INTEGER,
      name: DataTypes.STRING,
      price: DataTypes.BIGINT,
      description: DataTypes.TEXT,
      publish: DataTypes.BOOLEAN,
      soldAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "Product",
    }
  );
  return Product;
};
