"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Wishlist extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Wishlist.belongsTo(models.Product, {
        foreignKey: "productId",
        as: "Product",
      });

      Wishlist.belongsTo(models.User, {
        foreignKey: "userId",
        as: "User",
      });
    }
  }
  Wishlist.init(
    {
      productId: DataTypes.INTEGER,
      userId: DataTypes.INTEGER,
      deletedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "Wishlist",
    }
  );
  return Wishlist;
};
