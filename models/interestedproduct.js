"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class InterestedProduct extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      InterestedProduct.hasMany(models.Notifactions, {
        foreignKey: "InterestedProductId",
        as: "InterestedProductNotifactions",
      })
      // define association here
      InterestedProduct.belongsTo(models.Product, {
        foreignKey: "productId",
        as: "Product",
      });

      InterestedProduct.belongsTo(models.User, {
        foreignKey: "buyerId",
        as: "Buyer",
      });
    }
  }
  InterestedProduct.init(
    {
      productId: DataTypes.INTEGER,
      buyerId: DataTypes.INTEGER,
      price: DataTypes.INTEGER,
      agreement: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "InterestedProduct",
    }
  );
  return InterestedProduct;
};
