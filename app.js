const express = require("express");
const app = express();
const cors = require("cors");
const errorHanling = require("./helpers/errorHandling.helper");
const routes = require("./routes");
const morgan = require("morgan");
const swaggerUi = require("swagger-ui-express");

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(morgan("dev"));
app.use(express.static("public"));
app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(require("./SwaggerSecondHand.json"))
);

app.use(routes);

app.use(errorHanling);

module.exports = app;
