'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Notifactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      // userId: {
      //   type: Sequelize.INTEGER
      // },
      userIdBuyer: {
        type: Sequelize.INTEGER
      },
      userIdSeller: {
        type: Sequelize.INTEGER
      },
      productId: {
        type: Sequelize.INTEGER
      },
      InterestedProductId: {
        type: Sequelize.INTEGER
      },
      Redirect: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.BIGINT,
      },
      status: {
        type: Sequelize.STRING
      },
      WatchedAt: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Notifactions');
  }
};