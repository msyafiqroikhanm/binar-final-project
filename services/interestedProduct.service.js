const {
  InterestedProduct,
  Product,
  User,
  ProductImage,
  ProductCategory,
  Category,
} = require("../models");
const { Op } = require("sequelize");

const offerPrice = async (price) => {
  return await InterestedProduct.create(price);
};

const getInterestedProduct = async (where) => {
  return await InterestedProduct.findOne({
    where,
    include: [
      { model: Product, as: "Product" },
      {
        model: User,
        as: "Buyer",
        attributes: [
          "id",
          "email",
          "nama",
          "kota",
          "no_hp",
          "profile_picture",
          "completed",
        ],
      },
    ],
  });
};

const updateInterestedProduct = async (form, where) => {
  return await InterestedProduct.update(form, { where });
};

const getAllInterestedProducts = async (userId) => {
  return await InterestedProduct.findAll({
    where: { agreement: { [Op.or]: [null, true] } },
    include: [
      {
        model: Product,
        as: "Product",
        where: {
          userId,
          soldAt: null,
          publish: true,
        },
        include: [
          // {
          //   model: User,
          //   as: "User",
          //   attributes: [
          //     "id",
          //     "email",
          //     "nama",
          //     "kota",
          //     "no_hp",
          //     "profile_picture",
          //     "completed",
          //   ],
          // },
          {
            model: ProductImage,
            as: "ProductImage",
            attributes: ["id", "image"],
          },
          {
            model: ProductCategory,
            as: "ProductCategory",
            attributes: ["id"],
            include: {
              model: Category,
              as: "Category",
              attributes: ["id", "name"],
            },
          },
        ],
      },
    ],
  });
};

const getDetailInterestedProduct = async (where, userId) => {
  return await InterestedProduct.findOne({
    where,
    include: [
      {
        model: User,
        as: "Buyer",
        attributes: [
          "id",
          "email",
          "nama",
          "kota",
          "no_hp",
          "profile_picture",
          "completed",
        ],
      },
      {
        model: Product,
        as: "Product",
        where: {
          userId,
          soldAt: null,
        },
        include: [
          {
            model: User,
            as: "User",
            attributes: ["nama", "kota", "profile_picture"],
          },
          {
            model: ProductImage,
            as: "ProductImage",
            attributes: ["id", "image"],
          },
          {
            model: ProductCategory,
            as: "ProductCategory",
            attributes: ["id"],
            include: {
              model: Category,
              as: "Category",
              attributes: ["id", "name"],
            },
          },
        ],
      },
    ],
  });
};

module.exports = {
  offerPrice,
  getAllInterestedProducts,
  getDetailInterestedProduct,
  getInterestedProduct,
  updateInterestedProduct,
};
