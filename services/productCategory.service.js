const { ProductCategory } = require("../models");

const createProductCategory = async (form) => {
  return await ProductCategory.bulkCreate(form);
};

const updateProductCategory = async (form, where) => {
  await ProductCategory.destroy({ where });
  await createProductCategory(form);
};

module.exports = { createProductCategory, updateProductCategory };
