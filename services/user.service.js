const { User } = require("../models");

const getUserBy = async (where) => {
  const user = await User.findOne({
    where,
    attributes: [
      "id",
      "nama",
      "email",
      "kota",
      "alamat",
      "no_hp",
      "profile_picture",
    ],
  }).catch((err) => console.log(err));
  return user;
};

const getDataUser = async (where) => {
  return await User.findOne({ where });
};

const createUser = async (form) => {
  return await User.create(form);
};

const updateUser = async (form, where) => {
  return await User.update(form, { where });
};

module.exports = { getUserBy, createUser, updateUser, getDataUser };
