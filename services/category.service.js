const { Category } = require("../models");

const getAllCategories = async () => {
  return Category.findAll({ attributes: ["id", "name"] });
};

module.exports = { getAllCategories };
