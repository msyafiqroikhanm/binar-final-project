const { ProductImage } = require("../models");

const createImage = async (form) => {
  return await ProductImage.bulkCreate(form);
};

const udpateProductImage = async (form, where) => {
  await ProductImage.destroy({ where });
  await createImage(form);
};

module.exports = { createImage, udpateProductImage };
