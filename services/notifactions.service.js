const {
  Notifactions,
  Product,
  InterestedProduct,
  ProductImage,
} = require("../models");
const { Op } = require("sequelize");
const getNotifOne = async (where) => {
  return await Notifactions.findOne({
    where,
    attributes: [
      "id",
      "userIdBuyer",
      "userIdSeller",
      "status",
      "WatchedAt",
      "createdAt",
      "Redirect"
    ],
  })
};
const getNotif = async (where) => {
  return await Notifactions.findAll({
    where,
    attributes: [
      "id",
      "userIdBuyer",
      "userIdSeller",
      "status",
      "WatchedAt",
      "createdAt",
      "Redirect"
    ],
    include: [
      {
        model: Product,
        as: "NotifactionsProduct",
        attributes: ["id", "name", "price"],
        include: {
          model: ProductImage,
          as: "ProductImage",
          attributes: ["image"],
        },
      },
      {
        model: InterestedProduct,
        as: "NotifactionsInterestedProduct",
        attributes: ["id", "price"],
        include: {
          model: Product,
          as: "Product",
          attributes: ["id", "name", "price"],
          include: {
            model: ProductImage,
            as: "ProductImage",
            attributes: ["image"],
          },
        },
      },
    ],
    order: [["id", "DESC"]],
  });
};

const postNotif = async (form, where) => {
  return await Notifactions.create(form, { where });
};

const SetReadNotif = async (form, where) => {
  return await Notifactions.update(form, { where });
};

const SetReadAllNotif = async (form, where) => {
  return await Notifactions.update(form, { where });
};

const GetNotifNull = async (userid) => {
  return await Notifactions.findAll({
    where: {
      WatchedAt: { [Op.eq]: null },
      [Op.or]: [{ userIdBuyer: userid }, { userIdSeller: userid }],
    },
    order: [["id", "DESC"]],
    attributes: ["id", "status", "createdAt"],
    include: [
      {
        model: Product,
        as: "NotifactionsProduct",
        attributes: ["id", "name", "price"],
        include: {
          model: ProductImage,
          as: "ProductImage",
          attributes: ["image"],
        },
      },
      {
        model: InterestedProduct,
        as: "NotifactionsInterestedProduct",
        attributes: ["id", "price"],
        include: {
          model: Product,
          as: "Product",
          attributes: ["id", "name", "price"],
          include: {
            model: ProductImage,
            as: "ProductImage",
            attributes: ["image"],
          },
        },
      },
    ],
  });
};
module.exports = {
  getNotif,
  postNotif,
  SetReadNotif,
  GetNotifNull,
  SetReadAllNotif,
  getNotifOne
};

