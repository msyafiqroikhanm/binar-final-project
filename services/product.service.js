const {
  Product,
  ProductCategory,
  Category,
  User,
  ProductImage,
  InterestedProduct,
  Wishlist,
} = require("../models");
const { createImage } = require("./productImage.service");
const {
  createProductCategory,
} = require("../services/productCategory.service");

const getAllProducts = async (where, limit, offset, whereCategory) => {
  return await Product.findAll({
    limit: limit || null,
    offset: offset || null,

    order: [["id", "ASC"]],
    where,
    attributes: ["id", "name", "price", "description", "publish", "soldAt"],
    include: [
      {
        model: ProductImage,
        as: "ProductImage",
        attributes: ["id", "image"],
      },
      {
        model: ProductCategory,
        as: "ProductCategory",
        where: whereCategory,
        attributes: ["id"],
        include: {
          model: Category,
          as: "Category",
          attributes: ["id", "name"],
        },
      },
    ],
  });
};

const getAllProductByUser = async (id) => {
  const products = Product.findAll({
    where: { userId: id },
    include: [
      {
        model: InterestedProduct,
        as: "Interested",
      },
      {
        model: ProductImage,
        as: "ProductImage",
        attributes: ["id", "image"],
      },
      {
        model: ProductCategory,
        as: "ProductCategory",
        attributes: ["id"],
        include: {
          model: Category,
          as: "Category",
          attributes: ["id", "name"],
        },
      },
    ],
  });
  return products;
};

const getAllProductBy = async (where) => {
  const products = Product.findAll({
    where,
    include: [
      {
        model: InterestedProduct,
        as: "Interested",
      },
      // {
      //   model: User,
      //   as: "User",
      //   attributes: [
      //     "id",
      //     "nama",
      //     "kota",
      //     "no_hp",
      //     "profile_picture",
      //     "completed",
      //   ],
      // },
      {
        model: ProductImage,
        as: "ProductImage",
        attributes: ["id", "image"],
      },
      {
        model: ProductCategory,
        as: "ProductCategory",
        attributes: ["id"],
        include: {
          model: Category,
          as: "Category",
          attributes: ["id", "name"],
        },
      },
    ],
  });
  return products;
};

const getDetailProductBy = async (where, whereInterested) => {
  return await Product.findOne({
    where,
    include: [
      {
        model: User,
        as: "User",
        attributes: [
          "id",
          "nama",
          "kota",
          "no_hp",
          "profile_picture",
          "completed",
        ],
      },
      {
        model: ProductImage,
        as: "ProductImage",
        attributes: ["id", "image"],
      },
      {
        model: ProductCategory,
        as: "ProductCategory",
        attributes: ["id"],
        include: {
          model: Category,
          as: "Category",
          attributes: ["id", "name"],
        },
      },
      {
        model: InterestedProduct,
        as: "Interested",
        where: whereInterested,
      },
      {
        model: Wishlist,
        as: "Wishlist",
      },
    ],
  });
};

const createProduct = async (formProduct, formImage, formProductCategory) => {
  const newProduct = await Product.create(formProduct);

  formImage.map((element) => {
    element.productId = newProduct.id;
  });
  await createImage(formImage);

  formProductCategory.map((element) => {
    element.productId = newProduct.id;
  });
  console.log(formProductCategory);
  await createProductCategory(formProductCategory);

  return newProduct;
};

const updatePublish = async (where) => {
  return await Product.update({ publish: true }, { where });
};

const updateProduct = async (form, where) => {
  await Product.update(form, { where });
};

const softDeleteProduct = async (form, where) => {
  await Product.update(form, { where });
}

const hardDeleteProdcut = async (where) => {
  return await Product.destroy({where});
} 

module.exports = {
  getAllProductByUser,
  getDetailProductBy,
  createProduct,
  updatePublish,
  updateProduct,
  getAllProductBy,
  getAllProducts,
  softDeleteProduct,
  hardDeleteProdcut
};
