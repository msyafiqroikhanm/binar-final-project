const {
  Wishlist,
  Product,
  ProductImage,
  ProductCategory,
  Category,
  User,
} = require("../models");
const { getUserBy } = require("./user.service");

const createWishlist = async (form) => {
  return await Wishlist.create(form);
};

const getAllWishlists = async (where) => {
  return await Wishlist.findAll({
    where,
    include: [
      {
        model: Product,
        as: "Product",
        include: [
          {
            model: ProductImage,
            as: "ProductImage",
            attributes: ["id", "image"],
          },
          {
            model: ProductCategory,
            as: "ProductCategory",
            attributes: ["id"],
            include: {
              model: Category,
              as: "Category",
              attributes: ["id", "name"],
            },
          },
        ],
      },
    ],
  });
};

const getDetailWishlist = async (where) => {
  return await Wishlist.findOne({
    where,
    include: [
      {
        model: Product,
        as: "Product",
        include: [
          {
            model: ProductImage,
            as: "ProductImage",
            attributes: ["id", "image"],
          },
          {
            model: ProductCategory,
            as: "ProductCategory",
            attributes: ["id"],
            include: {
              model: Category,
              as: "Category",
              attributes: ["id", "name"],
            },
          },
        ],
      },
      {
        model: User,
        as: "User",
        attributes: [
          "id",
          "email",
          "nama",
          "no_hp",
          "kota",
          "alamat",
          "profile_picture",
        ],
      },
    ],
  });
};

const softDeleteWishlist = async (form, where) => {
  return await Wishlist.update(form, { where });
};

module.exports = {
  createWishlist,
  getAllWishlists,
  getDetailWishlist,
  softDeleteWishlist,
};
