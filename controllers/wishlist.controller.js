const { getDetailProductBy } = require("../services/product.service");
const responseFormatter = require("../helpers/responseFormatter.helper");
const {
  getNotif,
  postNotif,
  GetNotifNull,
} = require("../services/notifactions.service");
const { Op } = require("sequelize");
const {
  createWishlist,
  getAllWishlists,
  getDetailWishlist,
  softDeleteWishlist,
} = require("../services/wishlist.service");

class WishlistController {
  static async createWishlist(req, res, next) {
    try {
      const product = await getDetailProductBy({
        id: req.body.productId,
        publish: true,
      });
      if (!product) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Produk tidak ditemukan",
        };
      }

      if (product.soldAt != null) {
        throw {
          code: 410,
          status: "Gone",
          message: "Produk sudah terjual",
        };
      }

      if (product.userId === req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Produk milik anda sendiri",
        };
      }

      const wishlists = await getAllWishlists({
        userId: req.user.id,
        productId: product.id,
        deletedAt: null,
      });

      if (wishlists.length > 0) {
        throw {
          code: 409,
          status: "Conflict",
          message: "Anda sudah memasukan produk ini ke wishlist sebelumnya",
        };
      }

      const newWishlist = await createWishlist({
        productId: req.body.productId,
        userId: req.user.id,
      });

      const SendNotification = await postNotif(
        {
          userIdBuyer: req.user.id,
          productId: req.body.productId,
          Redirect: "wishlist",
          status: "Wishlist Product",
        },
        { userIdBuyer: req.user.id }
      );

      responseFormatter.success(
        res,
        201,
        "Created",
        "Berhasil menambahkan ke wishlist"
      );
    } catch (error) {
      next(error);
    }
  }

  static async listWishlist(req, res, next) {
    try {
      const listWishlist = await getAllWishlists({
        userId: req.user.id,
        deletedAt: null,
      });
      if (listWishlist.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Wishlist kamu kosong",
        };
      }

      const Notifikasi = await GetNotifNull(req.user.id);

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Wishlist ditemukan",
      {listWishlist,
      Notifikasi}
      );
    } catch (error) {
      next(error);
    }
  }

  static async detailWishlist(req, res, next) {
    try {
      const wishlist = await getDetailWishlist({
        id: req.params.id,
        deletedAt: null,
      });
      if (!wishlist) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Wishlist tidak ditemukan",
        };
      }

      if (wishlist.userId != req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Kamu tidak berhak melihat wishlist ini",
        };
      }

      const Notifikasi = await GetNotifNull(req.user.id);

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Wishlist ditemukan",
        {wishlist,
        Notifikasi}
      );
    } catch (error) {
      next(error);
    }
  }

  static async deleteWishlist(req, res, next) {
    try {
      const wishlist = await getDetailWishlist({
        id: req.params.id,
        deletedAt: null,
      });
      if (!wishlist) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Wishlist tidak ditemukan",
        };
      }

      if (wishlist.userId != req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Kamu tidak berhak melihat wishlist ini",
        };
      }

      await softDeleteWishlist({ deletedAt: new Date() }, { id: wishlist.id });

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Wishlist berhasil dihapus"
      );
    } catch (error) {
      next(error);
    }
  }

  static async deleteWishlistByProduct(req, res, next) {
    try {
      const wishlist = await getDetailWishlist({
        productId: req.params.id,
        userId: req.user.id,
        deletedAt: null,
      });
      if (!wishlist) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Wishlist tidak ditemukan",
        };
      }

      await softDeleteWishlist({ deletedAt: new Date() }, { id: wishlist.id });

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Wishlist berhasil dihapus"
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = WishlistController;
