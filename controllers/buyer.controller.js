const {
  getDetailProductBy,
  getAllProducts,
} = require("../services/product.service");
const { postNotif, GetNotifNull } = require("../services/notifactions.service");
const { offerPrice } = require("../services/interestedProduct.service");
const responseFormatter = require("../helpers/responseFormatter.helper");
const { Op } = require("sequelize");

class BuyerController {
  static async getAllProducts(req, res, next) {
    try {
      let where = { publish: true, soldAt: null };
      if (req.user) {
        where = {
          publish: true,
          soldAt: null,
          [Op.not]: {
            userId: req.user.id,
          },
        };
      }

      let limit = null,
        offset = null;
      let whereCategory;

      if (req.query) {
        offset = (req.query.page - 1) * req.query.size;
        limit = req.query.size;

        if (req.query.search) {
          where.name = {
            [Op.iLike]: `%${req.query.search}%`,
          };
        }

        if (req.query.category) {
          if (typeof req.query.category == "string") {
            req.query.category = [req.query.category];
            whereCategory = {
              categoryId: {
                [Op.in]: req.query.category,
              },
            };
          } else if (typeof req.query.category == "undefined") {
            whereCategory = null;
          }
        }
      }

      const products = await getAllProducts(
        where,
        limit,
        offset,
        whereCategory
      );
      if (products.length === 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Produk lagi kosong",
        };
      }

      let Notifikasi;
      if (req.user) {
        Notifikasi = await GetNotifNull(req.user.id);
      }

      return responseFormatter.success(res, 200, "OK", "Daftar Produk", {
        products,
        Notifikasi,
      });
    } catch (error) {
      next(error);
    }
  }

  static async getProduct(req, res, next) {
    try {
      const products = await getDetailProductBy({ id: req.params.id });
      if (!products) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Product tidak ditemukan.",
        };
      }

      // console.log(products.Interested.length);
      let cekPenawaran;
      if (products.Interested.length > 0) {
        cekPenawaran = true;
      } else {
        cekPenawaran = false;
      }

      let cekWishlist = false;
      products.Wishlist.forEach((element) => {
        console.log(element.deletedAt);
        if (element.userId == req.user.id && element.deletedAt == null) {
          cekWishlist = true;
        }
      });

      const Notifikasi = await GetNotifNull(req.user.id);

      return responseFormatter.success(res, 200, "OK", "Detail Produk", {
        products,
        cekPenawaran,
        cekWishlist,
        Notifikasi,
      });
    } catch (error) {
      next(error);
    }
  }

  static async offerPrice(req, res, next) {
    try {
      const products = await getDetailProductBy(
        {
          id: req.params.id,
        }
        // { buyerId: req.user.id, agreement: false }
      );
      if (!products) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Product yang anda tawar tidak ditemukan.",
        };
      }

      let cekPenawaran = false;
      products.Interested.forEach((element) => {
        // console.log(element);
        if (element.buyerId == req.user.id && element.agreement != false) {
          cekPenawaran = true;
        }
      });
      // console.log(cekPenawaran);

      if (cekPenawaran) {
        throw {
          code: 409,
          status: "Conflict",
          message: "Penawaran anda sudah sebelumnya diproses",
        };
      }
      // return res.send(products);
      const price = await offerPrice({
        price: req.body.price,
        productId: req.params.id,
        buyerId: req.user.id,
      });

      await postNotif(
        {
          userIdBuyer: req.user.id,
          InterestedProductId: req.params.id,
          status: "Penawaran Produk",
          Redirect: "detailProduct"
        },
        { userIdBuyer: req.user.id }
      );

      await postNotif(
        {
          userIdSeller: products.User.id,
          InterestedProductId: req.params.id,
          status: "Produk Ditawar",
          Redirect: "interestedProduct"
        },
      );

      responseFormatter.success(
        res,
        201,
        "Created",
        "Harga tawarmu berhasil dikirim ke penjual"
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = BuyerController;
