const { updateUser, getUserBy } = require("../services/user.service");
const responseFormatter = require("../helpers/responseFormatter.helper");
const cloudinary = require("../helpers/cloudinary.helper");
const {
  SetReadNotif,
  SetReadAllNotif,
  GetNotifNull,
} = require("../services/notifactions.service");
const fs = require("fs");
const { validationResult } = require("express-validator");
const ValidationMiddleware = require("../middlewares/validation.middleware");
class UserController {
  static async updateUser(req, res, next) {
    try {
      let errors = [];
      if (req.file) {
        const public_id = `second-hand-assets/profile/${req.file.filename}`;
        const cdn = await cloudinary.uploader.upload(req.file.path, {
          public_id,
        });
        req.body.profile_picture = cdn.secure_url;
        fs.unlinkSync(req.file.path);
      } else {
        if (!req.body.nama) {
          errors.push("Atribut nama tidak boleh kosong");
        }
        if (!req.body.kota) {
          errors.push("Atribut kota tidak boleh kosong");
        }
        if (!req.body.alamat) {
          errors.push("Atribut alamat tidak boleh kosong");
        }
        if (!req.body.no_hp) {
          errors.push("Atribut no_hp tidak boleh kosong");
        }

        if (errors.length > 0) {
          throw {
            code: 400,
            status: "Bad Request",
            message: errors,
          };
        }
      }

      const user = await updateUser(
        {
          nama: req.body.nama,
          kota: req.body.kota,
          alamat: req.body.alamat,
          no_hp: req.body.no_hp,
          profile_picture: req.body.profile_picture,
          completed: true,
        },
        {
          id: req.user.id,
        }
      );

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Anda berhasil memperbarui akun"
      );
    } catch (error) {
      next(error);
    }
  }

  static async getDetailUser(req, res, next) {
    try {
      const getUser = await getUserBy(req.user.id);
      const Notifikasi = await GetNotifNull(req.user.id);
      return responseFormatter.success(
        res,
        200,
        "OK",
        "User Detail",
        getUser,
        Notifikasi
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = UserController;
