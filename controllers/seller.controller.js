const {
  getAllProductByUser,
  getDetailProductBy,
  updatePublish,
  getAllProductBy,
  updateProduct,
} = require("../services/product.service");
const {
  getNotif,
  postNotif,
  GetNotifNull,
} = require("../services/notifactions.service");
const {
  getAllInterestedProducts,
  getDetailInterestedProduct,
  getInterestedProduct,
  updateInterestedProduct
} = require("../services/interestedProduct.service");
const responseFormatter = require("../helpers/responseFormatter.helper");
const { createProduct, hardDeleteProdcut } = require("../services/product.service");
require("dotenv").config();
const path = `${process.env.BASE_URL}:${process.env.PORT}/`;
const { Op } = require("sequelize");
const cloudinary = require("../helpers/cloudinary.helper");
const fs = require("fs");
const {
  updateProductCategory,
} = require("../services/productCategory.service");
const { udpateProductImage } = require("../services/productImage.service");

class SellerController {
  static async getProducts(req, res, next) {
    try {
      let products;
      if (!req.query.filter) {
        const allProducts = await getAllProductByUser(req.user.id);
        products = allProducts;
        if (products.length === 0) {
          throw {
            code: 404,
            status: "Not Found",
            message: "Anda tidak memiliki produk yang dijual.",
          };
        }
      }

      if (req.query.filter == "interested") {
        products = await getAllInterestedProducts(req.user.id);
        if (products.length === 0) {
          throw {
            code: 404,
            status: "Not Found",
            message:
              "Belum ada produkmu yang diminati nih, sabar ya rejeki gak kemana kok",
          };
        }
      }

      if (req.query.filter == "sold") {
        products = await getAllProductBy({
          userId: req.user.id,
          publish: true,
          soldAt: {
            [Op.not]: null,
          },
        });

        if (products.length === 0) {
          throw {
            code: 404,
            status: "Not Found",
            message:
              "Belum ada produkmu yang terjual nih, sabar ya rejeki gak kemana kok",
          };
        }
      }

      const Notifikasi = await GetNotifNull(req.user.id);

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Data produk anda sudah ditemukan.",
        { products, Notifikasi }
      );
    } catch (error) {
      next(error);
    }
  }

  static async createProduct(req, res, next) {
    try {
      let formImage = [];

      for (let i = 0; i < req.files.length; i++) {
        const cdn = await cloudinary.uploader.upload(req.files[i].path, {
          public_id: `second-hand-assets/products/${req.files[i].filename}`,
        });
        formImage.push({ image: cdn.secure_url });
        fs.unlinkSync(req.files[i].path);
      }

      if (typeof req.body.categories == "string") {
        req.body.categories = [req.body.categories];
      }

      let formProductCategory = req.body.categories.map((element) => {
        return {
          categoryId: element,
        };
      });

      const newProduct = await createProduct(
        {
          userId: req.user.id,
          name: req.body.name,
          price: req.body.price,
          description: req.body.description,
          publish: req.body.publish,
        },
        formImage,
        formProductCategory
      );

      if (req.body.publish == "true") {
        const SendNotification = await postNotif(
          {
            userIdSeller: req.user.id,
            productId: newProduct.id,
            Redirect: "allProduct",
            status: "Publish",
          },
          { userIdSeller: req.user.id }
        );
        return responseFormatter.success(
          res,
          201,
          "Created",
          "Produk berhasil diterbitkan"
        );
      }

      const detailProduct = await getDetailProductBy({ id: newProduct.id });

      return responseFormatter.success(
        res,
        201,
        "Created",
        "Produk berhasil dibuat",
        detailProduct
      );
    } catch (err) {
      next(err);
    }
  }

  static async updateProduct(req, res, next) {
    try {
      //cek data
      const detailProduct = await getDetailProductBy({
        id: req.params.id,
        soldAt: null,
      });

      if (!detailProduct) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Produk tidak ditemukan",
        };
      }

      if (detailProduct.userId !== req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Anda tidak berhak mengubah product",
        };
      }

      //update categories
      if (req.body.categories) {
        if (typeof req.body.categories == "string") {
          req.body.categories = [req.body.categories];
        }

        let formProductCategory = req.body.categories.map((element) => {
          return {
            productId: detailProduct.id,
            categoryId: element,
          };
        });

        await updateProductCategory(formProductCategory, {
          productId: detailProduct.id,
        });
      }

      // update images
      let formImage = [];
      if (req.files.length != 0) {
        for (let i = 0; i < req.files.length; i++) {
          const cdn = await cloudinary.uploader.upload(req.files[i].path, {
            public_id: `second-hand-assets/products/${req.files[i].filename}`,
          });
          formImage.push({
            productId: detailProduct.id,
            image: cdn.secure_url,
          });
          fs.unlinkSync(req.files[i].path);
        }

        await udpateProductImage(formImage, { productId: detailProduct.id });
      }

      //update product
      await updateProduct(
        {
          name: req.body.name || detailProduct.name,
          price: req.body.price || detailProduct.price,
          description: req.body.description || detailProduct.description,
        },
        { id: detailProduct.id }
      );

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Produk berhasil diperbarui"
      );
    } catch (error) {
      next(error);
    }
  }

  static async updatePublish(req, res, next) {
    try {
      const detailProduct = await getDetailProductBy({
        id: req.params.id,
        soldAt: null,
      });

      if (!detailProduct) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Produk tidak ditemukan",
        };
      }

      if (detailProduct.userId !== req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Anda tidak berhak mengubah product",
        };
      }

      await updatePublish({ id: req.params.id });

      const SendNotification = await postNotif(
        {
          userIdSeller: req.user.id,
          productId: detailProduct.id,
          price: detailProduct.price,
          Redirect: "allProduct",
          status: "Berhasil Diterbitkan",
        },
        { userIdSeller: req.user.id }
      );

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Produk berhasil diterbitkan.",
        SendNotification
      );
    } catch (error) {
      next(error);
    }
  }

  static async getDetailProduct(req, res, next) {
    try {
      const detailProduct = await getDetailProductBy({
        id: req.params.id,
      });
      if (!detailProduct) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Produk tidak ditemukan",
        };
      }

      const Notifikasi = await GetNotifNull(req.user.id);

      return responseFormatter.success(res, 200, "OK", "Produk ditemukan", {
        detailProduct,
        Notifikasi,
      });
    } catch (error) {
      next(error);
    }
  }

  static async getDetailInterestedProduct(req, res, next) {
    try {
      const detailInterestedProduct = await getDetailInterestedProduct(
        {
          id: req.params.id,
        },
        req.user.id
      );
      if (!detailInterestedProduct) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Data penawaran tidak ditemukan",
        };
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Data detail produk ditemukan",
        detailInterestedProduct
      );
    } catch (error) {
      next(error);
    }
  }

  static async transactionInterestedProduct(req, res, next) {
    try {
      const interestedProduct = await getInterestedProduct({
        id: req.params.id,
      });
      if (!interestedProduct) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Penawaran tidak ditemukan",
        };
      }

      if (interestedProduct.Product.userId !== req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized request",
          message: "Anda tidak berhak atas penawaran ini",
        };
      }

      if (req.body.agreement == true) {
        await updateInterestedProduct(
          { agreement: false },
          {
            id: { [Op.not]: interestedProduct.id },
            productId: interestedProduct.productId,
          }
        );
      }
      await updateInterestedProduct(req.body, { id: interestedProduct.id });

      let product;
      if (req.body.agreement) {
        product = await getInterestedProduct({ id: interestedProduct.id });
        await postNotif({
          userIdBuyer: interestedProduct.buyerId,
          InterestedProductId: req.params.id,
          status: "Penawaran disetujui",
          Redirect: "interestedProduct",
        });

        await postNotif({
          userIdSeller: interestedProduct.Product.userId,
          InterestedProductId: req.params.id,
          status: "Penawaran disetujui",
          Redirect: "detailProduct",
        });
      }

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Berhasil melakukan transaksi penawaran",
        product
      );
    } catch (error) {
      next(error);
    }
  }

  static async updateStatusProduct(req, res, next) {
    try {
      const interestedProduct = await getInterestedProduct({
        id: req.params.id,
      });
      if (!interestedProduct) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Penawaran tidak ditemukan",
        };
      }

      if (interestedProduct.Product.userId !== req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized request",
          message: "Anda tidak berhak atas penawaran ini",
        };
      }

      if (!req.body.status) {
        await updateInterestedProduct(
          { agreement: null },
          { id: interestedProduct.id }
        );
      } else {
        await updateProduct(
          { soldAt: new Date() },
          { id: interestedProduct.Product.id }
        );
        await postNotif(
          {
            userIdBuyer: interestedProduct.buyerId,
            userIdSeller: interestedProduct.Product.userId,
            InterestedProductId: req.params.id,
            status: "Produk Terjual",
            Redirect: "productSold",
          },
          { userIdSeller: req.user.id }
        );
      }

      const updated = await getInterestedProduct({
        id: interestedProduct.id,
      });

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Status produk berhasil diperbarui",
        updated
      );
    } catch (error) {
      next(error);
    }
  }

  static async deleteProduct(req, res, next) {
    try {
      const product = await getDetailProductBy({
        id: req.params.id,
      });

      if (!product) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Produk tidak ditemukan",
        };
      }

      if (product.userId != req.user.id) {
        throw {
          code: 401,
          status: "Unauthorized",
          message: "Kamu tidak berhak menghapus produk ini",
        };
      }

      await hardDeleteProdcut({ id: product.id });

      return responseFormatter.success(
        res,
        200,
        "OK",
        "Produk berhasil dihapus",
      );
    }
    catch (error) {
      next(error);
    }
  }

  static async getNotifactionsNull(req, res, next) {
    try {
      const Notifikasi = await GetNotifNull(req.user.id);
      return responseFormatter.success(
        res,
        200,
        "OK",
        "Notifactions",
        Notifikasi
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = SellerController;
