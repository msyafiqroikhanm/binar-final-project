const {
  getUserBy,
  createUser,
  getDataUser,
} = require("../services/user.service");
const bcrypt = require("bcryptjs");
const responseFormatter = require("../helpers/responseFormatter.helper");
const { generateJwt } = require("../services/jwt.service");
const googleHelper = require("../helpers/google.helper");
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(
  process.env.GOOGLE_CLIENT_ID,
  process.env.GOOGLE_CLIENT_SECRET
);
class AuthController {
  static async register(req, res, next) {
    try {
      const checkUser = await getUserBy({ email: req.body.email });
      if (checkUser) {
        throw {
          code: 409,
          status: "Conflict",
          message: "Email sudah digunakan",
        };
      }

      const salt = bcrypt.genSaltSync(10);
      const hashedPassword = bcrypt.hashSync(req.body.password, salt);
      req.body.password = hashedPassword;
      req.body.completed = false;
      req.body.login_type = "basic";
      req.body.profile_picture =
        "https://res.cloudinary.com/syafiq/image/upload/v1657626946/second-hand-assets/profile/seed-data/default.png";

      const newUser = await createUser(req.body);
      return responseFormatter.success(
        res,
        201,
        "Created",
        "Anda berhasil melakukan pendaftaran"
      );
    } catch (error) {
      next(error);
    }
  }

  static async oauth(req, res, next) {
    try {
      const { code } = req.query;

      if (!code) {
        // generate url for google login
        const loginUrl = googleHelper.generateAuthUrl();

        // redirect to google login url
        return res.send({ loginUrl });
      }

      await googleHelper.setCredentials(code);

      const { data } = await googleHelper.getUserInfo();

      const user = {
        name: data.given_name,
        email: data.email,
        login_type: "google-oauth2",
        completed: false,
        access_token
      };

      const checkUser = await getDataUser({ email: user.email });

      // if (!checkUser) {
      //   await createUser(user);
      // } else if (checkUser.login_type === "basic") {
      //   throw {
      //     code: 401,
      //     status: "Unauthorized Request",
      //     message: "Login dengan tipe basic",
      //   };
      // }

      const access_token = await generateJwt({
        id: user.id,
      });

      return res.status(200).json({
        status: "OK",
        message: "User found",
        data: user,
      });
    } catch (error) {
      next(error);
    }
  }

  static async googleOauth(req, res, next) {
    let token;
    const google = await client.verifyIdToken({
      idToken: req.body.id_token,
      audiance: process.env.GOOGLE_CLIENT_ID,
    });

    const user = await getUserBy({
      email: google.payload.email,
      login_type: "google-oauth2",
    });

    if (user) {
      token = await generateJwt({
        id: user.id,
        email: user.email,
      });
    } else {
      const createdUser = await createUser({
        email: google.payload.email,
        nama: google.payload.given_name + " " + google.payload.family_name,
        login_type: "google-oauth2",
        profile_picture:
          "https://res.cloudinary.com/syafiq/image/upload/v1657626946/second-hand-assets/profile/seed-data/default.png",
      });

      token = await generateJwt({
        id: createdUser.id,
        email: createdUser.email,
      });
    }

    return responseFormatter.success(res, 200, "OK", "Berhasil Login", {
      access_token: token,
    });
  }

  static async login(req, res, next) {
    try {
      const user = await getDataUser({ email: req.body.email });
      if (!user) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Email atau password salah",
        };
      }

      if (!bcrypt.compareSync(req.body.password, user.password)) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Email atau password salah",
        };
      }

      const access_token = await generateJwt({
        id: user.id,
      });

      return responseFormatter.success(res, 200, "OK", "Berhasil Login", {
        access_token,
      });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = AuthController;
