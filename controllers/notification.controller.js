const { Op } = require("sequelize");
const ResponseFormatter = require("../helpers/responseFormatter.helper");
const {
  getNotif,
  SetReadNotif,
  SetReadAllNotif,
  getNotifOne,
} = require("../services/notifactions.service");

class NotifactionController {
  static async getNotifactions(req, res, next) {
    try {
      let notifications;
      let where = {
        [Op.or]: [{ userIdBuyer: req.user.id }, { userIdSeller: req.user.id }],
      };

      if (req.query) {
        if (req.query.read == "false") {
          where.WatchedAt = { [Op.eq]: null };
        }
      }

      notifications = await getNotif(where);

      if (notifications.length == 0) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Notifikasi kosong",
        };
      }
      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Notifactions",
        notifications
      );
    } catch (error) {
      next(error);
    }
  }

  static async ReadNotification(req, res, next) {
    try {
      const notification = await getNotifOne({
        id: req.params.id,
      });
      if (!notification) {
        throw {
          code: 404,
          status: "Not Found",
          message: "Notifikasi kosong",
        };
      }
      if (
        notification.userIdBuyer != req.user.id &&
        notification.userIdSeller != req.user.id
      ) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Anda Tidak Berhak Atas Data Ini",
        };
      } else {
        await SetReadNotif(
          {
            WatchedAt: new Date(),
          },
          {
            id: req.params.id,
          }
        );
      }
      return ResponseFormatter.success(res, 200, "OK", "Notifikasi terbaca");
    } catch (error) {
      next(error);
    }
  }

  static async ReadAllNotification(req, res, next) {
    try {
      const Read = await SetReadAllNotif(
        {
          WatchedAt: new Date(),
        },
        {
          [Op.or]: [
            { userIdBuyer: req.user.id },
            { userIdSeller: req.user.id },
          ],
        }
      );
      return ResponseFormatter.success(
        res,
        200,
        "OK",
        "Notifikasi terbaca semua"
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = NotifactionController;
