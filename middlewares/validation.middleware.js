const { validationResult } = require("express-validator");

class ValidationMiddleware {
  static async result(req, res, next) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        next({
          code: 400,
          status: "Bad Request",
          message: errors.errors,
        });
      }
      next();
    } catch (error) {
      next(error);
    }
  }

  static async singleFile(req, res, next) {
    try {
      if (!req.file) {
        next({
          code: 400,
          status: "Bad Request",
          message: "File harus diisi",
        });
      }
      next();
    } catch (error) {
      next(error);
    }
  }

  static async multipleFile(req, res, next) {
    try {
      if (req.files.length == 0) {
        next({
          code: 400,
          status: "Bad Request",
          message: "Beberapa file harus diisi",
        });
      }
      next();
    } catch (error) {
      next(error);
    }
  }
}

module.exports = ValidationMiddleware;
