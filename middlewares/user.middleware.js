const { getUserBy } = require("../services/user.service");

class UserMiddleware {
  static async checkCompleteUserInfo(req, res, next) {
    try {
      const user = await getUserBy({
        id: req.user.id,
        completed: true,
      });

      if (!user) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message:
            "Silahkan melengkapi data sebelum melakukan penjualan/pembelian.",
        };
      }

      next();
    } catch (error) {
      next(error);
    }
  }
}

module.exports = UserMiddleware;
