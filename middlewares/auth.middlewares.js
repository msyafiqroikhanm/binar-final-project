const { verifyJwt } = require("../services/jwt.service");
const { getUserBy } = require("../services/user.service");

class AuthMiddleware {
  static async authentication(req, res, next) {
    try {
      if (!req.headers.authorization) {
        throw {
          code: 406,
          status: "Not Acceptable",
          message: "Anda tidak memiliki access_token",
        };
      }

      const payload = await verifyJwt(req.headers.authorization);
      if (!payload) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Token tidak sah",
        };
      }

      const user = await getUserBy({ id: payload.id });
      console.log(user);

      if (!user) {
        throw {
          code: 401,
          status: "Unauthorized Request",
          message: "Token akses anda tidak valid",
        };
      }

      req.user = payload;
      console.log(req.user);
      next();
    } catch (error) {
      next(error);
    }
  }
}

module.exports = AuthMiddleware;
