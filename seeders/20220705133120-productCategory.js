"use strict";
const fs = require("fs");

module.exports = {
  async up(queryInterface, Sequelize) {
    const data = JSON.parse(
      fs.readFileSync("./seeders/data/productCategories.json")
    );

    const productCategories = data.map((element) => {
      return {
        productId: element.productId,
        categoryId: element.categoryId,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });

    await queryInterface.bulkInsert("ProductCategories", productCategories);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("ProductCategories", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};
