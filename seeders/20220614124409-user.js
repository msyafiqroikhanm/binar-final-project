"use strict";
const fs = require("fs");
const bcrypt = require("bcryptjs");

module.exports = {
  async up(queryInterface, Sequelize) {
    const data = JSON.parse(fs.readFileSync("./seeders/data/users.json"));
    const users = data.map((element) => {
      return {
        email: element.email,
        password: bcrypt.hashSync(element.nama, 10),
        nama: element.nama,
        kota: element.kota,
        alamat: element.alamat,
        no_hp: element.no_hp,
        profile_picture: element.profile_picture,
        login_type: element.login_type,
        completed: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });
    await queryInterface.bulkInsert("Users", users);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Users", null, {
      truncate: true,
      restartIdentity: true,
    });
  },
};
